<?php namespace App;

use Illuminate\Database\Eloquent\Model;
class Personal extends Model{
	/**
	 * nombre de la tabla que usara el modelo
	 *
	 * @var string
	 */
	protected $table = 'personal';

	/**
	 * campos a cargar de la tabla
	 *
	 * @var array
	 */
	protected $fillable = [];

}
