<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	/**
	 * solo retorna la vista principal
	 *
	 * @return retorna la vista de bienvenida
	 */
	public function index()
	{
		return view('home.index');
	}

}
