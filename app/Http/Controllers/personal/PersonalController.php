<?php namespace App\Http\Controllers\personal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Personal;
class PersonalController extends Controller{
	/**
	* Controlador encargado de la actividad del personal
	* =================================================
	*/



	/**
	 * Metodo que se encarga de mostrar el formulario par capturar
	 * ==========================================================
	 * @return retornamos la vista
	 */
	public function getAgregar()
	{

		return view('personal.agregar');
	}
	/**
	 * Metodo muestra carga los datos para la tabla del personal
	 * ========================================================
	 * @return retornamos la vista con los datos
	 */
	public function getLista()
	{
		$personal=Personal::where('Activo','1')->orderBy('created_at','desc')->paginate(15);
		return View('personal.lista',['result'=>$personal]);
	}
	/**
	 * Metodo muestra solo la vista del formulario para buscar
	 * ======================================================
	 * @return retornamos la vista
	 */
	public function getBuscar()
	{
		return View('personal.buscar');
	}
	/**
	 * Metodo hace la busqueda en la db con la mono-clave
	 * =================================================
	 * @return retornamos la vista con los datos
	 */
	public function postBuscar(Request $request)
	{
		$data=$request->only('MonoClave');
		$validator=[
			'MonoClave' => 'required|min:14',
		];
		$valida = \Validator::make($data, $validator);
		if($valida->passes()){
			$personal=Personal::where('Activo','1')->where('MonoClave',$data['MonoClave'])->first();
			if(count($personal)){
				return View('personal.buscar',['result'=>$personal]);
			}
			return redirect()->back()->withInput()->with(['NoResult'=>'No se encontraron registros']);
		}
		return redirect()->back()->withInput()->withErrors($valida->errors());
	}
	/**
	 * Metodo para agregar el personal a la base de datos
	 * =================================================
	 * @return retornamos errores y mensaje de insersion
	 */
	public function postAdd(Request $request)
	{
		$data=$request->all();
		$validator=[
			'Nombre'          => 'required|min:3',
			'ApellidoPaterno' => 'required|min:3',
			'ApellidoMaterno' => 'required|min:3',
			'Email'           => 'email',
			'FechaNacimiento' => 'min:9|max:10',
			'LugarNacimiento' => 'min:5',
			'EstadoCivil'     => 'required',
			'Telefono'        => 'min:7'
		];
		$valida = \Validator::make($data, $validator);
		if($valida->passes()){
			$personal=new Personal;

			$personal->Nombres         = trim($data['Nombre']);
			$personal->ApellidoPaterno = trim($data['ApellidoPaterno']);
			$personal->ApellidoMaterno = trim($data['ApellidoMaterno']);
			$personal->Email           = trim($data['Email']);
			$personal->FechaNacimiento = trim($data['FechaNacimiento']);
			$personal->LugarNacimiento = trim($data['LugarNacimiento']);
			$personal->EstadoCivil     = $data['EstadoCivil'];
			$personal->Telefono        = trim($data['Telefono']);

			if($personal->save()){


				$MonoClave[0]  = substr($personal->Nombres, 0, 2);
				$MonoClave[0] .= substr($personal->ApellidoPaterno, 0, 2);
				$MonoClave[0] .= substr($personal->ApellidoMaterno, 0, 1);

				$MonoClave_fecha = implode('',explode('-',$personal->FechaNacimiento));
				$MonoClave[1]    = substr($MonoClave_fecha, 2, 6);

				$MonoClave[2] = $personal->id;
				$strMonoClave = strtolower(implode('-',$MonoClave));

				$person_MonoClave = Personal::find($personal->id);

				$person_MonoClave->id        = $personal->id;
				$person_MonoClave->MonoClave = $strMonoClave;
				if($person_MonoClave->save()){
					$alertMsg = ['msj'   => "Los datos se guardaron correctamente, su Mono Clave es: $strMonoClave",
							 	 'class' => 'alert-success'];
				}else{
					$alertMsg = ['msj'   => 'ERROR al crear Mono Clave',
								 'class' => 'alert-danger'];
				}
			}else{
				$alertMsg = ['msj'   => 'Ho Ho, algo salio mal, vuelve a intentarlo!',
							 'class' => 'alert-danger'];
			}
			return redirect()->back()->with($alertMsg);
		}

		return redirect()->back()->withInput()->withErrors($valida->errors());
	}
	/**
	 * Metodo se ultiliza para "eliminar", solo cambia el status activo a 0
	 * ===================================================================
	 * @return retornamos errores y mensaje de ejecucion
	 */
	public function getEliminar($personal_id)
	{
		$personal=Personal::find($personal_id);

		if(!is_null($personal)){
			$personal->id     = $personal_id;
			$personal->Activo = 0;

			if($personal->save()){
				$alertMsg = ['msj'   => "$personal->Nombres se elimino correctamente",
						 	 'class' => 'alert-success'];
			}else{
				$alertMsg = ['msj'   => "$personal->Nombres no se ha eliminado, vuelve a intentar",
							 'class' => 'alert-danger'];
			}
		}else{
			$alertMsg = ['msj'   => 'Ho Ho, algo salio mal, no se ha eliminado, vuelve a intentar!',
						 'class' => 'alert-danger'];
		}
		return redirect()->back()->with($alertMsg);
	}
}