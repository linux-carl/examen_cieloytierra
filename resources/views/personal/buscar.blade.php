@extends('app')
@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading"><span class="glyphicon glyphicon-search"></span> Buscar por Mono Clave</div>
			<div class="panel-body">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> favor de verificar que la Mono Clave sea correcta.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				@if(Session::get('msj')&&Session::get('class'))
					<div class="alert alert-dismissible {{Session::get('class')}}" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong>{{Session::get('msj')}}</strong>
					</div>
				@endif
				{!!Form::open([
					'url'    => 'personal/buscar',
					'method' => 'POST',
					'name'   => 'FrmBuscarPersonal',
					'id'     => 'FrmBuscarPersonal'
				])!!}
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="input-group">
					      {!! Form::text('MonoClave','',['id'=>'MonoClave','title'=>'Mono Clave','required'=>'required','class'=>'form-control','placeholder'=>'Ejemplo: manez-861206-2']) !!}
					      <span class="input-group-btn">
					        {!! Form::button('Buscar',['class'=>'btn btn-primary','type'=>'submit']);!!}
					      </span>
					    </div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						@if(isset($result)&&count($result))
							<table class="table table-hover table-responsive table-striped">
					  			<tbody>
									<tr>
								  		<td><strong>Mono Clave:</strong> </td>
								  		<td>{{$result->MonoClave}}</td>
								  	</tr>
								  	<tr>
								  		<td><strong>Nombres:</strong> </td>
								  		<td>{{$result->Nombres}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Apellido Paterno:</strong></td>
								  		<td>{{$result->ApellidoPaterno}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Apellido Materno:</strong></td>
								  		<td>{{$result->ApellidoMaterno}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Fecha de Nacimiento:</strong></td>
								  		<td>{{$result->FechaNacimiento}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Lugar de Nacimiento:</strong></td>
								  		<td>{{$result->LugarNacimiento}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Estado Civil:</strong></td>
								  		<td>{{$result->EstadoCivil}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Email:</strong></td>
								  		<td>{{$result->Email}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Telefono:</strong></td>
								  		<td>{{$result->Telefono}}</td>
							  		</tr>
							  		<tr>
								  		<td><strong>Registro:</strong></td>
								  		<td>{{$result->created_at}}</td>
								  	</tr>
							 	</tbody>
							 </table>

						@elseif(Session::get('NoResult'))
								<div class="alert alert-dismissible alert-warning" role="alert">
								  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								  <strong>{{Session::get('NoResult')}}</strong>
								</div>
					  	@endif

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
