@extends('app')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-md-6">
						<span class="glyphicon glyphicon-list-alt"> </span> Personal
					</div>
					<div class="col-md-6 text-right">
						<a href="{{asset('personal/agregar')}}" class="btn btn-primary"> <span class="glyphicon glyphicon-plus"></span> Nuevo</a>
					</div>
				</div>
			</div>
			<div class="panel-body">
				@if(Session::get('msj')&&Session::get('class'))
					<div class="alert alert-dismissible {{Session::get('class')}}" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong>{{Session::get('msj')}}</strong>
					</div>
				@endif
				<section class="sectionToolBarUp">
			      <div class="row">
			        <div class="col-lg-12 text-right">
			        	{!!$result->render()!!}
			        </div>
			      </div>
			    </section>

				<section id="sectionPersonal">
					<table class="table table-hover table-responsive table-striped">
					  <thead>
					  	<th>Opciones</th>
					  	<th>Mono clave</th>
					  	<th>Nombre</th>
					  	<th>Apellidos</th>
					  	<th>Estado civil</th>
					  	<th>Email</th>
					  	<th>Telefono</th>
					  </thead>
					  <tbody>
					  	@if(count($result))
							@foreach($result as $row)
							  	<tr>
							  		<td>
							  			<a title="Eliminar a {{$row->Nombres}}" href="#" onclick="showAlert('{{asset('personal/eliminar')}}/{{$row->id}}','¿Desea eliminar a {{$row->Nombres.' '.$row->ApellidoPaterno.' '.$row->ApellidoMaterno}} de la base de datos?')">
							  				<span class="glyphicon glyphicon-remove"></span>
							  			</a>
							  		</td>
							  		<td> {{$row->MonoClave}} 	 	</td>
							  		<td> {{$row->Nombres}} 	 		</td>
							  		<td> {{$row->ApellidoPaterno.' '.$row->ApellidoMaterno}} 	</td>
							  		<td> {{$row->EstadoCivil}} 		</td>
							  		<td> {{$row->Email}} 			</td>
							  		<td> {{$row->Telefono}} 		</td>
							  	</tr>
					  		@endforeach
					  	@else
					  		<tr>
							  	<td colspan="7">
							  		<div class="alert alert-dismissible alert-danger" role="alert">
									  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									  <strong><span class="glyphicon glyphicon-exclamation-sign"></span> Sin registros</strong>
									</div>
								</td>
							</tr>
					  	@endif

					  </tbody>
					</table>
				</section>
			</div>
		</div>
	</div>
</div>
@endsection