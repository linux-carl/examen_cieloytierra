@extends('app')
@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-plus"></span> Capturar nuevo personal
			</div>
			<div class="panel-body">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> Algo salio mal con sus datos, favor de verificar que sean correctos.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				@if(Session::get('msj')&&Session::get('class'))
					<div class="alert alert-dismissible {{Session::get('class')}}" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong>{{Session::get('msj')}}</strong>
					</div>
				@endif
				{!!Form::open([
					'url'    => 'personal/add',
					'method' => 'POST',
					'name'   => 'FrmCapturaPersonal',
					'id'     => 'FrmCapturaPersonal'
				])!!}
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						    {!! Form::label('Nombre', 'Nombre')!!}
                			{!! Form::text('Nombre','',['id'=>'Nombre','title'=>'Nombres','required'=>'required','class'=>'form-control','placeholder'=>'Nombre']) !!}
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						  	{!! Form::label('ApellidoPaterno', 'Apellido Paterno')!!}
                			{!! Form::text('ApellidoPaterno','',['id'=>'ApellidoPaterno','title'=>'Apellido Paterno','required'=>'required','class'=>'form-control','placeholder'=>'Apellido Paterno']) !!}
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						  	{!! Form::label('ApellidoMaterno', 'Apellido Materno')!!}
                			{!! Form::text('ApellidoMaterno','',['id'=>'ApellidoMaterno','title'=>'Apellido Materno','required'=>'required','class'=>'form-control','placeholder'=>'Apellido Materno']) !!}
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						  	{!! Form::label('FechaNacimiento', 'Fecha de nacimiento')!!}
                			{!! Form::input('date','FechaNacimiento','',['id'=>'FechaNacimiento','title'=>'Fecha de nacimiento','required'=>'required','class'=>'form-control','placeholder'=>'Fecha de nacimiento']) !!}
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						  	{!! Form::label('LugarNacimiento', 'Lugar de nacimiento')!!}
                			{!! Form::text('LugarNacimiento','',['id'=>'LugarNacimiento','title'=>'Lugar de nacimiento','required'=>'required','class'=>'form-control','placeholder'=>'Lugar de nacimiento']) !!}
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						  	{!! Form::label('EstadoCivil', 'Estado civil')!!}
                			{!! Form::select('EstadoCivil',
                			['Soltero'=>'Soltero','Casado'=>'Casado','Union libre'=>'Union libre','Divorciado'=>'Divorciado'],'',
                			['id'=>'EstadoCivil','class'=>'form-control']) !!}
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						  	{!! Form::label('Telefono', 'Numero de telefono')!!}
                			{!! Form::input('tel','Telefono','',['id'=>'Telefono','title'=>'Numero de telefono','class'=>'form-control','placeholder'=>'Numero de telefono']) !!}
						</div>
					</div>

					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="form-group">
						  	{!! Form::label('Email', 'Correo electronico')!!}
                			{!! Form::input('email','Email','',['id'=>'Email','title'=>'Correo electronico','class'=>'form-control','placeholder'=>'Ejemplo: carlos@gmail.com']) !!}
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-xs-12 text-center">
						<hr>
						<div class="form-group">
						  	{!! Form::button('<span class="glyphicon glyphicon-ok"></span> Guardar y nuevo',['class'=>'btn btn-primary','type'=>'submit'])!!}
						  	<a href="{{asset('personal/lista')}}" class="btn btn-primary"> <span class="glyphicon glyphicon-arrow-left"></span> Regresar</a>
						</div>
					</div>
				</div>
				{!!Form::close()!!}
			</div>
		</div>
	</div>
</div>
@endsection