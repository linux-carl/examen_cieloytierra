<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personal', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Nombres',90);
			$table->string('ApellidoPaterno',50);
			$table->string('ApellidoMaterno',50);
			$table->string('LugarNacimiento',50);
			$table->date('FechaNacimiento',10);
			$table->string('EstadoCivil',50);
			$table->string('Telefono',20);
			$table->string('Email',50);
			$table->string('MonoClave',100);
			$table->tinyInteger('Activo')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
